Bringing own patch/laptop
Tuning intervals
Different scales
Whose driving
Keyboard
Control parameters all at once
Comparing skills
Programming a melody
Sensitivity via the gate
Hitting the frame
Blowing and scratching
Clipping
Beatboxing
Tuning via an app
Using paper for calculations
Planning each recompile
Clapping rhythms
How are you going to play it
Playing with a pen
Changing gain mapping
Changing the chord in real time
Limited control over values but lots of gesture control
Volume threshold based controls
Getting head around real time performance control
Explains concept on paper
Collaborative capabilities favored
Interactions that produce high frequencies
Sketching control flow
Person using computer receiving instructions and others creating / performing
Encourage to use the object list
Audio rate to control rate conversion
Control flow
Explaining objects and patterns
Have to decide parameters before compiling
Thinking through rather than playing
Talking over each other because can’t all show an idea at the same time
Design moves getting further apart over time and proportional to complexity
How is it going to work?
Confidence vs doubt
Division of Labour across FBS roles
Sketches on paper getting more explicit to translate ideas into code
Using editor patch to check something is working
Using help patches
Verifying patch similarity to idea
Checking before compiling
Unexpected output 
Weird delay effects
Print objects conflicting?
Whiteboard model diagram
Addition error sending a zero to delay
Absolute math function for audio to control mapping
Quickly map other mics to new mapping

